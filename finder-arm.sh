#!/bin/bash

VER=`curl https://www.libreoffice.org/download/download-libreoffice/  | /usr/local/bin/htmlq -t | grep "release notes" | head -1 | awk '{print $2}'`

NEWLOC=https://download.documentfoundation.org/libreoffice/stable/${VER}/mac/aarch64/LibreOffice_${VER}_MacOS_aarch64.dmg

if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi
