#!/bin/bash

#NEWLOC1=`curl -L https://www.libreoffice.org/download/download/ | /usr/local/bin/htmlq -a href a | grep mac-x86 | head -1`

#NEWLOCTEMP="https://www.libreoffice.org${NEWLOC1}"

#NEWLOC=`curl -L $NEWLOCTEMP  2>/dev/null | /usr/local/bin/htmlq -a href a | grep x86 | head -1`

#NEWLOC=`curl -L "https://www.libreoffice.org${NEWLOC1}"  2>/dev/null | /usr/local/bin/htmlq -a href a | grep x86 | head -1`

#https://www.libreoffice.org/download/download-libreoffice/?type=mac-x86_64&version=7.4.2&lang=en-US

VER=`curl https://www.libreoffice.org/download/download-libreoffice/  | /usr/local/bin/htmlq -t | grep "release notes" | head -1 | awk '{print $2}'`

NEWLOC=https://download.documentfoundation.org/libreoffice/stable/${VER}/mac/x86_64/LibreOffice_${VER}_MacOS_x86-64.dmg

if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi